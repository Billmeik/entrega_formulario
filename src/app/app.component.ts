import { Component } from '@angular/core';

import LiferayParams from '../types/LiferayParams'

declare const Liferay: any;
export class User {
	public name: string;
	public apellidos: string;
	public email: string;
	public reEmail: string;
	public namePers: string;
	public nif: string;
	public cp: string;
  }
@Component({
	templateUrl: 
		Liferay.ThemeDisplay.getPathContext() + 
		'/o/Crear/app/app.component.html'
})
export class AppComponent {
	params: LiferayParams;
	labels: any;
	model = new User();
	siteKey: string;

	constructor() {
		this.siteKey = '6LdBrBAaAAAAAOxBYNG4hPl7uBo0yFGny9nXQUoJ';
		this.labels = {        
			
			configuration: 'Configuration',
			
			portletNamespace: 'Portlet Namespace',
        	contextPath: 'Context Path',
			portletElementId: 'Portlet Element Id',
		}
	}
	onSubmit(form: { value: any; }) {
		console.log(form.value)
	  }

	get configurationJSON() {
		return JSON.stringify(this.params.configuration, null, 2);
	}
}
